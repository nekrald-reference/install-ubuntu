#!/usr/bin/env bash

set -uexo

tar -xvf xfig+fig2dev-3.2.9a.tar.xz
cd fig2dev-3.2.9a
./configure
make
sudo make install
cd ..

cd xfig-3.2.9a
./configure
make
sudo make install
cd ..

set +uexo
