#!/usr/bin/env bash
set -uexo pipefail

# YouCompleteMe - automatic code completion for Vim.
cd $HOME
sudo rm -rf YouCompleteMe

git clone https://github.com/Valloric/YouCompleteMe.git
cd $HOME/YouCompleteMe
git submodule update --init --recursive
set NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
nvm install 22
nvm use 22
./install.py --all --verbose
set +uexo 

