#!/usr/bin/env bash
#
version=14.2.0
n_jobs=8

set -uexo pipefail

sudo apt-get install build-essential -y
sudo apt-get install libmpfr-dev libgmp3-dev libmpc-dev -y
wget http://ftp.gnu.org/gnu/gcc/gcc-$version/gcc-$version.tar.gz
tar -xf gcc-$version.tar.gz
cd gcc-$version
./configure -v --build=x86_64-linux-gnu --host=x86_64-linux-gnu --target=x86_64-linux-gnu --prefix=/usr/local/gcc-$version --enable-checking=release --enable-languages=c,c++ --disable-multilib --program-suffix=-$version
make -j $n_jobs
sudo make install
cd ..
rm gcc-$version -rf
rm gcc-$version.tar.gz

set +uexo
