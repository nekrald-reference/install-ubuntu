#!/usr/bin/env bash
#
set -uexo

build_packages=(libncurses5-dev libgtk2.0-dev libatk1.0-dev  libcairo2-dev libx11-dev libxpm-dev 
    libxt-dev python2-dev python3-dev ruby-dev lua5.2 liblua5.2-dev libperl-dev git checkinstall)

for package in ${build_packages[@]}; do
    sudo apt-get install -y $package
done

vim_packages=(vim vim-runtime vim-common vim-doc vim-athena vim-gtk3 vim-gui-common)

for package in ${vim_packages[@]}; do
    sudo apt-get remove  -y $package
done


git clone https://github.com/vim/vim.git
cd vim
./configure --with-features=huge \
            --enable-multibyte \
            --enable-rubyinterp=yes \
            --enable-python3interp=yes \
            --with-python3-config-dir=$(python3-config --configdir) \
            --enable-perlinterp=yes \
            --enable-luainterp=yes \
            --enable-gui=gtk2 \
            --enable-cscope \
            --prefix=/usr/local

make VIMRUNTIMEDIR=/usr/local/share/vim/vim91 -j 8
sudo make install

sudo update-alternatives --install /usr/bin/editor editor /usr/local/bin/vim 1
sudo update-alternatives --set editor /usr/local/bin/vim
sudo update-alternatives --install /usr/bin/vi vi /usr/local/bin/vim 1
sudo update-alternatives --set vi /usr/local/bin/vim

cd ..
sudo rm -rf vim

for package in ${vim_packages[@]}; do
    sudo apt-mark hold $package
done

set +uexo 
