#!/usr/bin/env bash

version=16.2
n_jobs=8

set -uexo 

sudo apt-get install build-essential
wget https://ftp.gnu.org/gnu/gdb/gdb-$version.tar.gz
tar -xvzf gdb-$version.tar.gz
cd gdb-$version
./configure
make -j $n_jobs 
sudo make install
cd ..
rm -rf gdb-$version
rm gdb-$version.tar.gz

set +uexo

