#!/usr/bin/env bash

# KAddressBook: application for vcard base management.
sudo apt-get install kaddressbook

# Timekpr nExT: daily amount and margins time limits.
sudo add-apt-repository ppa:mjasnik/ppa
sudo apt-get update
sudo apt-get install timekpr-next

# Safeeyes: 2h-20min work-break application.
sudo add-apt-repository ppa:slgobinath/safeeyes
sudo apt-get update
sudo apt-get install safeeyes

