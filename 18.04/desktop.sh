#!/usr/bin/env bash
set -uexo pipefail


# Installing desktop applications.
desktop_setup_list=(graphviz texlive-full vlc 
        imagemagick gedit-latex-plugin 
	git-doc git-el git-gui pkg-config
        compizconfig-settings-manager 
        firefox pandoc powerline
	chkrootkit rkhunter goldendict 
	texlive-fonts-recommended 
        texlive-pictures texlive-latex-extra 
	hplip-gui doxygen-gui calibre keepassx
	geogebra classicmenu-indicator 
        ubuntu-restricted-extras unity-tweak-tool
	dkms ffmpeg djview okular
        rdesktop vim-latexsuite vim-gui-common vim-doc doxygen 
)
sudo apt-get install -f -y ${desktop_setup_list[@]}

# Virtualbox Installation.
sudo apt-get install -f -y virtualbox
sudo usermod -aG vboxusers $USER
sudo usermod -aG vboxsf $USER
sudo adduser $USER vboxusers
sudo adduser $USER vboxsf
sudo groupadd -f vboxusers
sudo groupadd -f vboxsf

# Old scanner support.
mkdir -p "/home/$USER/epson"
cp ./config/esfw52.bin /home/$USER/epson/
sudo cp ./config/snapscan.conf /etc/sane.d/snapscan.conf
sudo chmod +rw /etc/sane.d/snapscan.conf
sudo chown $USER /etc/sane.d/snapscan.conf
chmod +rw /etc/sane.d/snapscan.conf

