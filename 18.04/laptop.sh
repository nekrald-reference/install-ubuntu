#!/usr/bin/env bash
set -uexo pipefail

# To make sure touchpad works.
sudo cp ./config/grub /etc/default/grub
sudo chmod +rwx /etc/default/grub
sudo update-grub


