set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
set rtp+=~/.vim/bundle/YouCompleteMe
set rtp+=~/YouCompleteMe

call vundle#begin()

Plugin 'gmarik/vundle'
Plugin 'Valloric/YouCompleteMe'

call vundle#end()

filetype plugin indent on
let g:ycm_global_ycm_extra_conf='~/.vim/bundle/.ycm_extra_conf.py'
set showcmd
set iminsert=0
set imsearch=0
highlight lCursor guifg=NONE guibg=Cyan
set history=100
set ruler
set number
set background=light
color morning

fun! <SID>StripTrailingWhitespaces()
	let l = line(".")
	let c = col(".")
	%s/\s\+$//e
	call cursor(l, c)
endfun

set laststatus=2
set t_Co=256

autocmd FileType c,cpp,java,php,ruby,python autocmd BufWritePre <buffer> :call <SID>StripTrailingWhitespaces()

syntax on
set shiftwidth=4
set softtabstop=4
set expandtab
set autoindent
set smartindent
set smarttab
set autochdir

noremap § <esc>
inoremap § <esc>`^
vnoremap § <esc>gV
nnoremap § <esc>
onoremap § <esc>
lnoremap § <esc>
cnoremap § <C-C><esc>

