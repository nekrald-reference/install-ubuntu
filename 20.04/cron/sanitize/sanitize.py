#!/usr/bin/env python3
import argparse
import logging
import subprocess as sp
from datetime import datetime
from datetime import timedelta


def main():
    """ Main entry point. """
    uninstall_apps()
    kill_and_rm_processes()


def uninstall_apps() -> None:
    preset_list = [
            'google-chrome-stable',
            'google-chrome-beta',
            'google-chrome-unstable',
            'chromium-browser',
            'links',
            'lynx',
            'w3m',
            'elinks',
            'links2',
            'epiphany-browser',
            'wine',
            'discord',
            'seamonkey-mozilla-build',
            'roccat-tools',
            'vivaldi-stable',
            'vivaldi-unstable',
            'vivaldi-beta',
            'opera-stable',
            'opera-unstable',
            'opera-beta',
    ]
    call_list = list()
    for item in preset_list:
        remove_list = ['apt-get', 'remove', '-y', item]
        remove_list = ['snap', 'remove', item]
        purge_list = ['apt-get', 'purge', '-y', item]
        call_list += [remove_list, purge_list]

    for entry in call_list:
        process = sp.Popen(entry, stdout=sp.PIPE, stderr=sp.PIPE)
        process.communicate()
        process.wait()


def kill_and_rm_processes() -> None:
    PROCESS_LIST = [
        'google-chrome',
        'w3m',
        'elinks',
        'links2',
        'links',
        'lynx',
        'discord',
        'vivaldi',
        'omniweb',
        'edge',
        'seamonkey',
        'maxthon',
        'roccat',
        'gnome-browser',
        'epiphany-browser',
        'iexplore',
        'iexplorer',
        'wine'
    ]
    for item in PROCESS_LIST:
        process = sp.Popen([
            'killall', item], stdout=sp.PIPE, stderr=sp.PIPE)
        process.communicate()
        process.wait()

    for item in PROCESS_LIST:
        cmd = ['which', item]
        p = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE)
        output, error = p.communicate()
        p.wait()
        if len(output.strip()) > 0:
            cmd = ['rm', '-rf', output]
            p = sp.Popen(cmd, stdout=sp.PIPE, stderr = sp.PIPE)
            p.communicate()
            p.wait()


if __name__ == '__main__':
    main()
