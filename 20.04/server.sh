#!/usr/bin/env bash
set -uexo pipefail


# Package installations.
sudo apt-get update 
sudo apt-get upgrade -f -y --force-yes 
sudo apt-get dist-upgrade -f -y --force-yes

server_setup_list=(g++ make cmake nodejs rar libclang-dev 
	vim vim-scripts vim-runtime vim-common tmux screen 
	build-essential pwgen uuid-dev uuid-runtime xsel
        gitk gitweb git-cvs git-mediawiki git-svn npm
	swig mono-mcs htop mono-complete pastebinit r-base 
        r-base-dev libopenblas-dev octave bison flex 
        vim-addon-manager libsqlite3-dev libffi-dev
        google-perftools python3-pip
        vowpal-wabbit libfreetype6 libfreetype6-dev 
        python3-setuptools libpython3-dev 
	gcc-multilib libgoogle-perftools-dev zlib1g-dev 
        python3-scipy python3-numpy libgflags-dev
        python3-matplotlib python3-pip g++ gcc 
        libgflags-dev libbz2-dev subversion 
	git make cmake libgoogle-perftools-dev 
        liblbfgs-dev libboost-all-dev bison flex
	build-essential samba-common-bin 
	gfortran liblog4cpp5-dev
        libopenblas-dev libffi-dev google-perftools 
	python3-dev python3-setuptools libpython3-dev 
	python3-matplotlib python3-scipy python3-numpy 
	liblog4cpp5-dev libjsoncpp-dev
	secure-delete network-manager
        android-tools-adb android-tools-fastboot 
        openjdk-8-jre openjdk-8-jdk
        libcurl4-openssl-dev
)

sudo apt-get install -f -y ${server_setup_list[@]}


# Processing .rc files.
execution_place=$(pwd)
cd /$HOME
sudo rm -rf $HOME/.rc
ln -sf "$execution_place/.rc" "$HOME/.rc"
cd "$HOME/.rc"
. link.sh

cd "$execution_place"
set +uexo pipefail

# YouCompleteMe - automatic code completion for Vim.
if [ ! -d "$HOME/YouCompleteMe" ]; then
    cd $HOME
    git clone https://github.com/Valloric/YouCompleteMe.git
    cd $HOME/YouCompleteMe
    git submodule update --init --recursive
    ./install.py --clang-completer
fi

cd "$execution_place"

