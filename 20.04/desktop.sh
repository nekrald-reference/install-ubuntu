#!/usr/bin/env bash
set -uexo pipefail


# Installing desktop applications.
desktop_setup_list=(graphviz texlive-full vlc 
        imagemagick gedit-latex-plugin 
	git-doc git-el git-gui pkg-config
        compizconfig-settings-manager 
        firefox pandoc powerline
	chkrootkit rkhunter goldendict 
	texlive-fonts-recommended 
        texlive-pictures texlive-latex-extra 
	hplip-gui doxygen-gui calibre keepassx
	geogebra classicmenu-indicator gparted
        ubuntu-restricted-extras unity-tweak-tool
	dkms ffmpeg djview okular phantomjs 
        rdesktop vim-latexsuite vim-gui-common vim-doc doxygen 
        caffeine
)
sudo apt-get install -f -y ${desktop_setup_list[@]}


