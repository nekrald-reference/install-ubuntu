#!/usr/bin/env bash
set -uexo pipefail

# Old scanner support.
mkdir -p "/home/$USER/epson"
cp ./config/esfw52.bin /home/$USER/epson/
sudo cp ./config/snapscan.conf /etc/sane.d/snapscan.conf
sudo chmod +rw /etc/sane.d/snapscan.conf
sudo chown $USER /etc/sane.d/snapscan.conf
chmod +rw /etc/sane.d/snapscan.conf

