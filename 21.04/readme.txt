Overall Instruction.
1. server.sh -- All basic tools for CLI work.
2. desktop.sh -- All basic tools for GUI work.
3. snap.sh -- Installation of GUI tools via snap.
4. productivity.sh -- All basic productivity tools for GUI.
5. rootkits.sh -- Checking for rootkits.
6. virtualbox.sh -- Installation of virtualbox if needed.
7. personal.sh -- Installation of system with personal use in virtual machine (Telegram and other dangerous tools).
8. uninstall/work.sh or uninstall/personal.sh -- Uninstallation of games and other useless staff.

Work Laptop Instruction:
1. server.sh
2. desktop.sh
3. productivity.sh
4. snap.sh
5. uninstall/work.sh

Personal Virtualbox Instruction:
1. server.sh
2. productivity.sh
3. personal.sh
4. uninstall/personal.sh



