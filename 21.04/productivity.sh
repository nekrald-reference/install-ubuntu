#!/usr/bin/env bash

# Timekpr nExT: daily amount and margins time limits.
sudo apt-get install timekpr-next -y

# Safeeyes: 2h-20min work-break application.
sudo apt-get install safeeyes -y

