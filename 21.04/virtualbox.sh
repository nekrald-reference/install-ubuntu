#!/usr/bin/env bash
set -uexo pipefail

# Virtualbox Installation.
sudo apt-get install -f -y virtualbox
sudo usermod -aG vboxusers $USER
sudo usermod -aG vboxsf $USER
sudo adduser $USER vboxusers
sudo adduser $USER vboxsf
sudo groupadd -f vboxusers
sudo groupadd -f vboxsf


