setlocal autoindent
setlocal smartindent
setlocal nospell	
setlocal expandtab
setlocal shiftwidth=4
setlocal softtabstop=4
setlocal wildmenu
setlocal number
setlocal showcmd
setlocal ruler
setlocal number
setlocal smarttab

nmap <F5> :w<enter> :!python %<CR>
