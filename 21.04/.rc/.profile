# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

if [ -f /place/home/malysh/kaldi/tools/torch/install/bin/torch-activate ] ; then 
  . /place/home/malysh/kaldi/tools/torch/install/bin/torch-activate
fi


if [ -f /mnt/storage/malysh/kaldi/tools/torch/install/bin/torch-activate ] ; then
    . /mnt/storage/malysh/kaldi/tools/torch/install/bin/torch-activate
fi

if [ -f  /home/malysh/torch/install/bin/torch-activate ] ; then
    . /home/malysh/torch/install/bin/torch-activate
fi

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/home/malysh/.sdkman"
[[ -s "/home/malysh/.sdkman/bin/sdkman-init.sh" ]] && source "/home/malysh/.sdkman/bin/sdkman-init.sh"
