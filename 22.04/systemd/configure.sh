#!/usr/bin/env bash

sudo cp ./logind.conf /etc/systemd/logind.conf
sudo loginctl enable-linger nekrald

sudo cp ./system/nekrald-system-update.service /etc/systemd/system/nekrald-system-update.service
mkdir -p ~/.config/systemd/user/
cp ./user/nekrald-user-update.service ~/.config/systemd/user/nekrald-user-update.service

sudo systemctl enable --now nekrald-system-update.service
systemctl --user enable --now nekrald-user-update.service


