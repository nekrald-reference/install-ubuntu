#!/usr/bin/env bash

set -uex

username=nekrald
export HOME=/home/$username
export PATH=$PATH:/home/$username/.dotnet

/home/$username/dotnet-install.sh --channel 8.0
dotnet tool update -g git-credential-manager

flatpak update --assumeyes

set +uex
