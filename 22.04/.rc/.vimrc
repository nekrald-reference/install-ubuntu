set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
set rtp+=~/.vim/bundle/aw-watcher-vim
set rtp+=~/YouCompleteMe

call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'ActivityWatch/aw-watcher-vim'
Plugin 'preservim/nerdtree'
Plugin 'github/copilot.vim'

call vundle#end()

filetype plugin indent on
let g:ycm_global_ycm_extra_conf='~/.vim/bundle/.ycm_extra_conf.py'
set showcmd
set iminsert=0
set imsearch=0
highlight lCursor guifg=NONE guibg=Cyan
set history=100
set ruler
set number
set background=light
color solarized

fun! <SID>StripTrailingWhitespaces()
	let l = line(".")
	let c = col(".")
	%s/\s\+$//e
	call cursor(l, c)
endfun

set laststatus=2
set t_Co=256

autocmd FileType c,cpp,java,php,ruby,python autocmd BufWritePre <buffer> :call <SID>StripTrailingWhitespaces()

syntax on
set shiftwidth=4
set softtabstop=4
set expandtab
set autoindent
set smartindent
set smarttab
set autochdir

noremap § <esc>
inoremap § <esc>`^
vnoremap § <esc>gV
nnoremap § <esc>
onoremap § <esc>
lnoremap § <esc>
cnoremap § <C-C><esc>

set colorcolumn=100

" Exit Vim if NERDTree is the only window remaining in the only tab.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

" Start NERDTree. If a file is specified, move the cursor to its window.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * NERDTree | if argc() > 0 || exists("s:std_in") | wincmd p | endif

python3 from powerline.vim import setup as powerline_setup
python3 powerline_setup()
python3 del powerline_setup
set laststatus=2

autocmd ColorScheme solarized highlight CopilotSuggestion guifg=#555555 ctermfg=8
let b:copilot_enabled = v:true
