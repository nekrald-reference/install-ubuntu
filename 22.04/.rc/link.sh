#!/usr/bin/env bash
set -uex

if [ -d /usr/share/vim/vim82 ]; then
	sudo ln -sf $HOME/.rc/russian-typewriter.vim /usr/share/vim/vim82/keymap/
	chmod +rw /usr/share/vim/vim82/keymap/russian-typewriter.vim
fi

cd $HOME 
rm -rf .vimrc
ln -sf .rc/.vimrc
rm -rf .gitconfig
ln -sf .rc/.gitconfig
rm -rf .vim
ln -sf .rc/.vim 
rm -rf .screenrc
ln -sf .rc/.screenrc
rm -rf .bashrc
ln -sf .rc/.bashrc
rm -rf .bash_aliases
ln -sf .rc/.bash_aliases
rm -rf .profile
ln -sf .rc/.profile
mkdir -p .ssh
cd .ssh
rm -rf .ssh/rc
ln -sf ../.rc/.ssh/rc
rm -rf .ssh/config
ln -sf ../.rc/.ssh/config
chmod 644 $HOME/.ssh/config

cd $HOME/.rc
git submodule update --init --recursive

cd $HOME 
wget --no-check-certificate https://raw.github.com/seebi/dircolors-solarized/master/dircolors.ansi-light
mv dircolors.ansi-light .dircolors
eval "$(dircolors -b $HOME/.dircolors)"

set +uex
