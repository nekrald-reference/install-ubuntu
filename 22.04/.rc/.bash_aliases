alias pbcopy='xsel --clipboard --input'
alias pbpaste='xsel --clipboard --output'
alias explore='nautilus .'
alias cp='cp -r'
alias scp='scp -r'
alias df='df -h'
alias du='du -h'
alias links='ls'
alias lynx='ls'
alias links2='ls'
alias elinks='ls'
alias w3m='ls'
alias disable-touchpad='xinput set-prop 12 "Device Enabled" 0'
alias enable-touchpad='xinput set-prop 12 "Device Enabled" 1'
alias python=python3

findfile() {
    local regex="$1"
    
    local open_files
    open_files=$(lsof 2> /dev/null | awk '{print $NF}' | egrep "$regex")
    local number_of_open_files
    number_of_open_files=$(printf "%s\n" "$open_files" | wc -l)

    if [[ -z "$open_files" ]] 
    then
        echo "No open files match the regex" >&2 
        return
    fi
    if [[ "$number_of_open_files" -gt 1 ]] 
    then
        echo "More than one open file match the regex" >&2
        return
    fi
    
    # now there is only one open file
    echo -n "$open_files"
}

getsize() {
    local filename="$1"
    
    lsof_output=$(lsof -s -o 0 "$filename" 2> /dev/null | awk '{ print $7 }')
    lines=$(printf "%s\n" "$lsof_output" | wc -l)

    if [[ $lines != 2 ]] 
    then
        echo -n "-1"
        return
    fi

    echo -n "$(printf "%s\n" "$lsof_output" | tail -1)"
}

getoffset() {
    local filename="$1"
    
    lsof_output=$(lsof -o -o 0 "$filename" 2> /dev/null | awk '{ print $7 }' | sed 's/^0t//g')
    lines=$(printf "%s\n" "$lsof_output" | wc -l)

    if [[ $lines != 2 ]] 
    then
        echo -n "-1"
        return
    fi

    echo -n "$(printf "%s\n" "$lsof_output" | tail -1)"
}

pecread() {
    regex="$1"
    filename=$(findfile "$regex")
    if [[ -z $filename ]] 
    then
        return
    fi


    filesize=$(getsize "$filename")

    percents=""
    while :
    do
        fileoffset=$(getoffset "$filename")
        if [[ "$fileoffset" == "-1" ]] 
        then
            break
        fi

        if [[ -n "$percents" ]]
        then
            echo -e -n "\033[2K"
        fi

        percents=$(echo "scale=2; $fileoffset * 100 / $filesize" | bc -l)"%"
        printf "\r%s:\t%s" "$filename" "$percents"

        if [[ -n "$2" ]]
        then
            sleep "$2"
        else
            break
        fi
    done
    echo
}

watchfile() {
    pecread "$1" 1
}

export -f pecread
export -f watchfile

# Colorized prompt
if [ $UID != 0  ] ; then
	PS1="\[\e[32;1m\]\u@\[\e[31;1m\]\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\\$ "
else
	PS1="\[\e[31;1m\]\u@\[\e[31;1m\]\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\\$ "
fi

# Colorized grep
export GREP_OPTIONS="--color=auto"
export GREP_COLOR="1;32"


