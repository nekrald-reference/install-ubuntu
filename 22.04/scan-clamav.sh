#!/usr/bin/env bash

# Clam AV scan
clamscan --recursive -i --exclude-dir="^/sys" --exclude-dir="/home/nekrald/hotels" --exclude-dir="/home/nekrald/sea" --exclude-dir="/media/nekrald/smith-exfat" --exclude-dir="/media/nekrald/smith-ext4" --exclude-dir="/home/nekrald/recsys" --exclude-dir="/home/nekrald/references" --exclude-dir="/usr/local/maldetect*" --exclude-dir="/home/nekrald/esicup" --exclude-dir="/home/nekrald/thesis" --exclude="/usr/sbin/chkrootkit" --exclude-dir="/usr/share/clamav-testfiles" --exclude-dir="/var/lib/clamav" --bell /



