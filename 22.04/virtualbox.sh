#!/usr/bin/env bash
set -uexo pipefail

# Virtualbox Installation.

wget -O- https://www.virtualbox.org/download/oracle_vbox_2016.asc | sudo gpg --yes --output /usr/share/keyrings/oracle-virtualbox-2016.gpg --dearmor

sudo apt-get update
sudo apt-get install -f -y virtualbox-7.1

sudo groupadd -f vboxusers
sudo groupadd -f vboxsf
sudo usermod -aG vboxusers $USER
sudo usermod -aG vboxsf $USER
sudo adduser $USER vboxusers
sudo adduser $USER vboxsf

