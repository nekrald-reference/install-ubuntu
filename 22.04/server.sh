#!/usr/bin/env bash
set -uexo pipefail


# Package installations.
sudo apt-get update 
sudo apt-get upgrade -f -y --force-yes 
sudo apt-get dist-upgrade -f -y --force-yes

server_setup_list=(android-tools-fastboot 
        android-tools-adb 
        bison 
        build-essential 
        cargo 
        check 
        chkrootkit 
        clamav 
        clamav-base 
        clamav-daemon 
        clamav-docs 
        clamav-freshclam 
        clamav-milter 
        clamav-testfiles 
        clamdscan
        cmake 
        flex
        g++ 
        gcc 
        gcc-multilib 
        gfortran 
        git 
        git-cvs 
        git-mediawiki 
        git-svn 
        gitk 
        gitweb 
        golang 
        golang-go
        google-perftools 
        htop 
        libboost-all-dev 
        libbz2-dev 
        libcanberra-gtk-module 
        libcanberra-gtk3-module
        libclamav-dev 
        libclamav9 
        libclamunrar
        libclamunrar9 
        libclang-dev 
        libcurl4-openssl-dev 
        libffi-dev 
        libfontconfig-dev
        libfontconfig1-dev
        libfreetype6 
        libfreetype6-dev 
        libgflags-dev
        libgflags-dev 
        libgoogle-perftools-dev 
        libjson-c-dev 
        libjsoncpp-dev 
        liblbfgs-dev 
        liblog4cpp5-dev 
        libmilter-dev 
        libncurses5-dev 
        libopenblas-dev 
        libpcre2-dev 
        libpython3-dev 
        libsqlite3-dev 
        libssl-dev 
        libwebkit2gtk-4.0-37
        libxml2-dev 
        make 
        mono-complete 
        network-manager
        nodejs 
        npm
        openjdk-17-jdk 
        openjdk-17-jre 
        pastebinit 
        pipx
        pkg-config 
        python3 
        python3-dev 
        python3-matplotlib 
        python3-numpy 
        python3-pip
        python3-pip 
        python3-pytest 
        python3-scipy 
        python3-setuptools 
        r-base 
        r-base-dev 
        rar 
        rkhunter 
        rustc 
        samba-common-bin 
        screen 
        subversion
        swig 
        uuid-dev 
        uuid-runtime 
        valgrind 
        vim 
        vim-addon-manager 
        vim-common 
        vim-nox
        vim-runtime 
        vim-scripts 
        xsel
        zlib1g-dev 
        img2pdf
        pdftk
)

sudo apt-get install -f -y ${server_setup_list[@]}


# Python Applications
pipx install zeal-feeds
pipx install academic

# Processing .rc files.
execution_place=$(pwd)
cd /$HOME
sudo rm -rf $HOME/.rc
ln -sf "$execution_place/.rc" "$HOME/.rc"
cd "$HOME/.rc"
git submodule update --init --recursive --remote
. link.sh
vim +PluginInstall +qall

cd "$execution_place"

# Modern NVM
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.0/install.sh | bash
. ~/.bashrc
set NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
nvm install 22

cd "$execution_place"

# .NET and Git Credential Manager
dotnet-install.sh --channel 7.0
dotnet tool install -g git-credential-manager
git-credential-manager configure

# Enable Firewall
sudo ufw enable

# Check for rootkits.
sudo rkhunter --check --skip-keypress
sudo chkrootkit

# Powerline
cd "$execution_place"
sudo apt-get install -f -y powerline fonts-powerline
git submodule update --init --recursive --remote
cd fonts
./install.sh
cd "$execution_place"

# Maldet
### Would require reconfiguring on scan frequency.
### config file: /usr/local/maldetect/conf.maldet

wget https://www.rfxn.com/downloads/maldetect-current.tar.gz
tar -xzf maldetect-current.tar.gz
rm -rf maldetect-current.tar.gz
cd maldetect-*
sudo ./install.sh
cd ..
rm -rf maldetect-*

set +uexo 

