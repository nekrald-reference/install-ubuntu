#!/usr/bin/env bash

set -uex

sudo apt-get update
sudo apt-get -y dist-upgrade
sudo apt -y -o APT::Get::Always-Include-Phased-Updates=true upgrade

sudo snap refresh
snap refresh --list

sudo flatpak update --assumeyes --system

sudo apt-mark showauto | xargs sudo apt-get install

sudo update-grub

set +uex
