#!/usr/bin/env bash

# Malware Detect for Linux
sudo maldet -u
sudo maldet -d
sudo maldet -a /var 
sudo maldet -a /tmp 
sudo maldet -a /dev/shm 
sudo maldet -a /srv 
sudo maldet -a /usr 
sudo maldet -a /bin 
sudo maldet -a /sbin 
sudo maldet -a /etc
sudo maldet -a /home 
# Chkrootkit
sudo chkrootkit

# Rootkit Hunter
sudo rkhunter --check

