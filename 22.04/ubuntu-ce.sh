#!/usr/bin/env bash

# For 22.04 specific.

set -uexo pipefail

wget https://job.ubuntuce.com/KEY.gpg && gpg --output ubuntuce.gpg --dearmor KEY.gpg && sudo mv ubuntuce.gpg /usr/share/keyrings/ && rm KEY.gpg

sudo apt install curl -y && sudo curl -s --compressed -o /etc/apt/sources.list.d/ubuntuce-jammy.list "https://job.ubuntuce.com/ubuntuce-jammy.list"

sudo apt-get update 
sudo apt-get install ubuntu-ce-software-center -y

sudo snap install dab-player

sudo apt-get install bibletime

set +uexo
