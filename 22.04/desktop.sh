#!/usr/bin/env bash
set -uexo pipefail

sudo ubuntu-drivers autoinstall
sudo apt-get install -f -y ubuntu-restricted-extras
timedatectl set-local-rtc 1

# Installing desktop applications.
desktop_setup_list=(bibtool
        caffeine 
        chromium-browser
        classicmenu-indicator 
        compizconfig-settings-manager 
        darktable
        djview 
        dkms 
        doxygen 
        doxygen-gui 
        evolution-ews
        ffmpeg 
        firefox 
        flatpak
        git-doc 
        git-gui 
        gnome-shell-extension-manager
        gnome-shell-extensions
        gnome-software-plugin-flatpak
        gnome-tweaks
        gnome-clocks
        gparted
        graphviz 
        gufw 
        menulibre
        pandoc 
        paprefs
        pavucontrol
        pkg-config
        plymouth-theme-ubuntu-gnome-logo 
        plymouth-theme-ubuntu-gnome-text 
        plymouth-theme-ubuntu-logo 
        plymouth-theme-ubuntu-text
        plymouth-themes
        potrace
        pulseffects # easyeffects
        pulseaudio-equalizer
        safeeyes
        texlive-fonts-recommended 
        texlive-full 
        texlive-latex-extra 
        texlive-pictures 
        timekpr-next 
        vim-doc 
        vim-gui-common 
        vim-latexsuite 
        vlc 
        zeal
        deepin-sound-theme
        pop-sound-theme
        sox
        xfig
        fig2dev
        libxft-dev
        libxaw7-dev
        libx11-dev
        libxrandr-dev
        libxdamage-dev
        mesa-common-dev
        libxcomposite-dev
        libxaw3dxft8-dev
        libxaw3dxft6
        xaw3dg-dev
        geogebra
        geogebra-gnome
        inkscape
)
sudo apt-get install -f -y ${desktop_setup_list[@]}

# Removing games and apps further installed with Snap.
app_remove_list=(gnome-sudoku gnome-mines sgt-launcher sgt-puzzles gnome-mahjongg aisleriot)
for apt_package in ${app_remove_list[@]}; do
    sudo apt-get remove $apt_package -y
done
sudo apt-get remove libreoffice-* -y

# Snap Applications
std_snap_setup_list=(canonical-livepatch 
                    canonical-livepatch-downloader 
                    chatgpt-desktop 
                    cheese
                    dev-docs-io-on-desktop
                    freetube
                    gimp
                    hugo
                    libreoffice
                    p3x-onenote
                    skype
                    teams-for-linux
                    xournalpp)
for snap_package in ${std_snap_setup_list[@]}; do
    sudo snap install $snap_package
done
classic_snap_setup_list=(code pycharm-community)
for snap_package in ${classic_snap_setup_list[@]}; do
    sudo snap install $snap_package --classic
done
snap_connect_list=(hugo gimp xournalpp libreoffice)
for snap_package in ${snap_connect_list[@]}; do
    sudo snap connect $snap_package:removable-media
done
 
# Flatpak Configuration.
flatpak remote-add --user --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
flatpack_package_list=(org.keepassxc.KeePassXC com.calibre_ebook.calibre)
for flatpak_package in ${flatpack_package_list[@]}; do
    sudo flatpak install --user flathub $flatpak_package
done

# GNOME extensions
pipx install gnome-extensions-cli --system-site-packages
pipx install poetry

gsettings set org.gnome.shell disable-extension-version-validation true

extension_list=(clipboard-indicator@tudmotu.com
    lockkeys@fawtytoo
    extension-list@tu.berry
    RecentItems@bananenfisch.net
    focused-window-dbus@flexagoon.com
    Vitals@CoreCoding.com
    gnomebedtime@ionutbortis.gmail.com
)
for extension_name in ${extensions[@]}; do
    gext install $extension_name
    gext enable $extension_name
done

gsettings set org.gnome.shell disable-extension-version-validation false
# For those incompatible, tweaking metadata.json helps.
# The extensions are located at $HOME/.local/share/gnome-shell/extensions.

# Installing ZOTERO
curl -sL https://raw.githubusercontent.com/retorquere/zotero-deb/master/install.sh | sudo bash
sudo apt-get update
sudo apt-get install zotero

