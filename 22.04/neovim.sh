#!/usr/bin/env bash

set -uexo 

sudo apt-get install software-properties-common
sudo apt-get install python3-dev python3-pip

curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz
sudo rm -rf /opt/nvim
sudo tar -C /opt -xzf nvim-linux64.tar.gz
rm -rf nvim-linux64.tar.gz

mkdir -p ~/.config/nvim/pack/github/start/
rm -rf ~/.config/nvim/pack/github/start/copilot.vim
git clone https://github.com/github/copilot.vim.git  ~/.config/nvim/pack/github/start/copilot.vim

set +uexo
