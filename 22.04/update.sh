#!/usr/bin/env bash


set -uex

./user-update.sh
./system-update.sh

set +uex
