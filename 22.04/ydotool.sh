#!/usr/bin/env bash

set -uexo

sudo apt-get install cmake scdoc pkg-config
sudo apt-get install checkinstall
sudo apt-get install git

git clone https://github.com/ReimuNotMoe/ydotool
cd ydotool
mkdir build && cd build
cmake ..
make -j `nproc`

export YDOTOOL_SOCKET=/run/user/$UID/.ydotool_socket
sudo checkinstall --fstrans=no
sudo cp ydotoold.service /lib/systemd/system/ydotoold.service

sudo systemctl daemon-reload
sudo systemctl enable ydotoold
sudo systemctl start ydotoold

cd ../../
rm -rf ydotool

set +uexo
