#!/usr/bin/env bash
set -uexo pipefail


sudo cp ./config/grub /etc/default/grub
sudo chmod +rwx /etc/default/grub
sudo update-grub

sudo apt-get update 
sudo apt-get upgrade -f -y --force-yes 
sudo apt-get dist-upgrade -f -y --force-yes

setup_list=(g++ make cmake graphviz nodejs texlive-full vlc rar 
    imagemagick gedit-latex-plugin libclang-dev screen xsel
    vim vim-scripts vim-doc vim-latexsuite gnome-tweak-tool
    vim-gui-common vim-gnome vim-runtime vim-common
    python-pip python-dev build-essential pwgen
    git-doc git-el git-gui pkg-config
    uuid-dev uuid-runtime tmux
    gitk gitweb  git-bzr git-cvs git-mediawiki git-svn 
    sni-qt python-matplotlib swig libgfortran3 mono-mcs 
    htop compizconfig-settings-manager 
    libboost-all-dev links lynx  pandoc powerline
    pastebinit npm firefox r-base 
    r-base-dev libopenblas-dev python-setuptools octave
    chkrootkit rkhunter goldendict 
    vim-youcompleteme vim-addon-manager
    texlive-fonts-recommended 
    texlive-pictures texlive-latex-extra 
    hplip-gui bison flex doxygen doxygen-gui 
    libsqlite3-dev libffi-dev
    geogebra classicmenu-indicator 
    google-perftools python3-pip
    gparted phantomjs ubuntu-restricted-extras 
    unity-tweak-tool vowpal-wabbit telegram-desktop
    libfreetype6 libfreetype6-dev python3-dev 
    python3-setuptools libpython3-dev
    calibre chromium-browser liblog4cpp5-dev 
    gcc-multilib libgoogle-perftools-dev zlib1g-dev 
    python3-scipy python3-numpy libgflags-dev
    python3-matplotlib python3-pip keepassx
    android-tools-adb android-tools-fastboot g++ gcc 
    libgflags-dev libbz2-dev subversion 
    git make cmake libgoogle-perftools-dev 
    liblbfgs-dev libboost-all-dev doxygen bison flex
    build-essential python-pip python-setuptools 
    gfortran python-dev liblog4cpp5-dev
    libopenblas-dev libffi-dev google-perftools 
    python3-dev python3-setuptools libpython3-dev 
    python3-matplotlib python3-scipy python3-numpy 
    liblog4cpp5-dev libjsoncpp-dev samba-common-bin 
    secure-delete rdesktop libgnome2-bin pdftk 
    dkms gnome-system-tools ffmpeg djview okular
)

sudo apt-get install -f -y ${setup_list[@]}

sudo add-apt-repository --yes ppa:webupd8team/java
sudo apt-get update

sudo apt-get install -f -y  oracle-java9-installer oracle-java9-set-default

# Only if virtualization is required.
sudo apt-get install -f -y virtualbox
sudo groupadd -f vboxusers
sudo usermod -aG vboxusers $USER
sudo adduser $USER vboxusers

mkdir -p "/home/$USER/epson"
cp ./config/esfw52.bin /home/$USER/epson/

sudo cp ./config/snapscan.conf /etc/sane.d/snapscan.conf
sudo chmod +rw /etc/sane.d/snapscan.conf
sudo chown $USER /etc/sane.d/snapscan.conf
chmod +rw /etc/sane.d/snapscan.conf


pip3 install --user --upgrade matplotlib numpy scipy sklearn pandas \
    django gym[atari,classic_control,box2d] jupyter[all] cvxopt \
    tqdm joblib zmq click bs4 selenium seaborn sns GPy statsmodels
pip3 install numdifftools --user --upgrade
pip3 install http://download.pytorch.org/whl/cpu/torch-0.3.1-cp36-cp36m-linux_x86_64.whl --user
pip3 install torchvision --user --no-depend

# Comment the following line if you do not want to install opera.
sudo add-apt-repository 'deb https://deb.opera.com/opera-stable/ stable non-free'
wget -qO- https://deb.opera.com/archive.key | sudo apt-key add -
sudo apt-get update
sudo apt-get install -f -y  opera-stable
sudo apt-get upgrade -f -y 
sudo apt-get dist-upgrade -f -y 

sudo apt-get -f -y  install
sudo snap install pycharm-community --classic
sudo snap install intellij-idea-community --classic --edge
sudo snap install slack --classic

git submodule update --init --recursive

execution_place=$(pwd)
cd /home/$USER
ln -sf "$execution_place/.rc" "/home/$USER/.rc"
cd "/home/$USER/.rc"
. link.sh

cd "$execution_place"

set +uexo pipefail

cd /home/$USER/
rm -rf YouCompleteMe
git clone https://github.com/Valloric/YouCompleteMe.git
mkdir -p .vim/bundle
cd .vim/bundle
ln -sf /home/$USER/YouCompleteMe
cd YouCompleteMe
git submodule update --init --recursive
./install.py --clang-completer

cd "$execution_place"

sudo rkhunter --check --skip-keypress
sudo chkrootkit

