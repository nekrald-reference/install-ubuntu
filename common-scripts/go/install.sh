#!/usr/bin/env bash

set -uexo pipefail

sudo apt-get remove golang-go
wget https://go.dev/dl/go1.24.1.linux-amd64.tar.gz
sudo rm -rf /usr/local/go 
sudo tar -C /usr/local -xzf go1.24.1.linux-amd64.tar.gz
rm -rf go1.24.1.linux-amd64.tar.gz

set +uexo

