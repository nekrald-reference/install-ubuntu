#!/usr/bin/env bash
#
set -uexo pipefail
version=19

wget -qO- https://apt.llvm.org/llvm.sh | sudo bash -s -- $version 

clang-$version --version

set +uexo
