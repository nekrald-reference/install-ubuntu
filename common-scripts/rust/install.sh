#!/usr/bin/env bash

set -uexo pipefail

sudo apt-get remove libstd-rust-dev libstd-rust* rustc cargo

curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

set +uexo

# Uninstall: rustup self uninstall
